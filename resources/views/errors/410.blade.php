@extends('errors::minimal')

@section('title', __('Link availability expired'))
@section('code', '410')
@section('message', __('Link availability expired'))