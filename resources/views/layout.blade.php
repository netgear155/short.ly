<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{ config('app.name') }} - URL shorter</title>

        <!-- Styles -->
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">

        <style>
            html, body {
                height: 100vh;
            }

            .wrapper {
                min-height: 100vh;
            }

            .logo-title {
                max-height: 100px;
            }
        </style>

        @stack('styles')
    </head>
    <body>
        <div class="container h-100">
            <div class="wrapper row align-items-center" id="app">
                <app file="{{ $file ?? null }}" address="{{ $address ?? null }}"></app>
            </div>
        </div>

        <script src="{{ mix('js/app.js') }}"></script>
        @stack('scripts')
    </body>
</html>
