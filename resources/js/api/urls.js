import axios from 'axios';

export default {
    // API Endpoint for search url by id
    find(id) {
        return axios.get(`/api/url/${id}`);
    },

    // API Endpoint for create url
    create(data) {
        return axios.post(`/api/url`, data);
    },
};