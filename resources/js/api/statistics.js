import axios from 'axios';

export default {
    // API Endpoint for get statistics with pagination
    paginateByUrl(id, params) {
        return axios.get(`/api/statistics/${id}`, {params});
    },

    paginate(params) {
        return axios.get(`/api/statistics`, {params});
    }

};