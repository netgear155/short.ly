import Vue from 'vue';
import VueRouter from "vue-router";

import Statistics from "../views/Shortly/Page/Statistics/Index";
import StatisticsSingle from "../views/Shortly/Page/Statistics/Single";
import Advertising from "../views/Shortly/Page/Advertising";
import Create from "../views/Shortly/Page/Create";

Vue.use(VueRouter);

export const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/statistics',
            name: 'statistics.index',
            component: Statistics
        },
        {
            path: '/statistics/:id',
            name: 'statistics.single',
            component: StatisticsSingle
        },
        {
            path: '/:code',
            name: "advertising",
            component: Advertising,
        },
        {
            path: '/',
            name: 'create',
            component: Create
        },
    ],
});