const state = {
    // The URL filename for showing to user
    file: null,
    // Address to which the user will be redirected
    address: null,
};

const getters = {
    FILE: state => {
        return state.file;
    },

    ADDRESS: state => {
        return state.address;
    },
};

const mutations = {
    SET_FILE: (state, payload) => {
        state.file = payload;
    },

    SET_ADDRESS: (state, payload) => {
        state.address = payload;
    },
};

const actions = {
};

export default {
    state,
    getters,
    mutations,
    actions,
};