import Vue from 'vue';
import Vuex from 'vuex';

import File from './modules/advertising'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    modules: {
        File,
    },
});