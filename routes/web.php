<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SpaController@index')->name('spa.index');

Route::get('/statistics', 'SpaController@index')->name('statistics.index');
Route::get('/statistics/{code}', 'SpaController@index')->name('statistics.single');

Route::get('/{code}', 'RedirectController@redirect')->name('shorturl.redirect');