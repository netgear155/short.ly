<?php

namespace App\Providers;

use App\Models\Url;
use App\Observers\UrlObserver;
use Gallib\ShortUrl\Hasher;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        $this->app->singleton(Hasher::class, function () {
            return new Hasher();
        });

        $this->app->alias(Hasher::class, 'hasher');

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Url::observe(UrlObserver::class);
    }
}
