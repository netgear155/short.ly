<?php

namespace App\Models;

use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Url
 *
 * @property int $id
 * @property string $url
 * @property string $code
 * @property string|null $title
 * @property string|null $description
 * @property int $counter
 * @property \Illuminate\Support\Carbon|null $expires_at
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Url newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Url newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Url query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Url whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Url whereCounter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Url whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Url whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Url whereExpiresAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Url whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Url whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Url whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Url whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Url whereUserId($value)
 * @mixin \Eloquent
 * @property int $commercial
 * @property-read string $shorter_url
 * @property-read string $statistics_url
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UrlStatistic[] $statistics
 * @property-read int|null $statistics_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Url whereCommercial($value)
 */
class Url extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'urls';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url',
        'code',
        'expires_at',
        'commercial'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'shorterUrl'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'expires_at',
    ];

    /**
     * The relationship for URL with statistics
     * @return HasMany
     */
    public function statistics(): HasMany
    {
        return $this->hasMany(UrlStatistic::class);
    }

    public function couldExpire(): bool
    {
        return $this->expires_at !== null;
    }

    /**
     * Return whether an url has expired.
     *
     * @return bool
     * @throws \Exception
     */
    public function hasExpired(): bool
    {
        if (! $this->couldExpire()) {
            return false;
        }

        $expiresAt = new Carbon($this->expires_at);

        return ! $expiresAt->isFuture();
    }

    /**
     * generate URL for redirecting user
     */
    public function getShorterUrlAttribute(): string
    {
        return route('shorturl.redirect', ['code' => $this->code]);
    }

    /**
     * Generate unique URL-statistics for this short URL
     */
    public function getStatisticsUrlAttribute(): string
    {
        return route('statistics.single', [
            'code' => (new Hashids('', 6))->encode($this->id)
        ]);
    }

}