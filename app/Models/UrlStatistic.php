<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UrlStatistic
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UrlStatistic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UrlStatistic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UrlStatistic query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $url_id
 * @property string|null $visitor_ip
 * @property int|null $visitor_unique
 * @property string|null $showed_commercial_file
 * @property mixed|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $browser
 * @property string|null $deviceType
 * @property string|null $device
 * @property string|null $platform
 * @property-read mixed $showed_commercial_image
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UrlStatistic whereBrowser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UrlStatistic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UrlStatistic whereDevice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UrlStatistic whereDeviceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UrlStatistic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UrlStatistic wherePlatform($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UrlStatistic whereShowedCommercialFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UrlStatistic whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UrlStatistic whereUrlId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UrlStatistic whereVisitorIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UrlStatistic whereVisitorUnique($value)
 */
class UrlStatistic extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url_id',
        'visitor_ip',
        'visitor_unique',
        'user_agent',
        'showed_commercial_file',
        'browser',
        'deviceType',
        'device',
        'platform'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at',
        'showed_commercial_file'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'showedCommercialImage'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i',
    ];

    public function getShowedCommercialImageAttribute() {
        return \Storage::disk('commercial-images')->url($this->showed_commercial_file);
    }
}
