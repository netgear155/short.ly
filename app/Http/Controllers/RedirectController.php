<?php

namespace App\Http\Controllers;

use App\Models\Url;
use \Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \Illuminate\View\View;
use Jenssegers\Agent\Agent;

class RedirectController extends Controller
{
    /**
     * Redirect to url by its code.
     *
     * @param string $code
     *
     * @param Request $request
     * @return View|RedirectResponse
     */
    public function redirect($code, Request $request)
    {
        // get url data from db
        $url = Url::whereCode($code)->first();

        if ($url !== null) {
            // check URL is expired
            if ($url->hasExpired()) {
                abort(410);
            }

            // Composer package for parse user-agent
            $agent = new Agent();

            // preparing data for create log about current request
            $statisticData = [
                'visitor_unique'    => true,
                'visitor_ip'        => $request->ip(),
                'device'            => $agent->device(),
                'platform'          => $agent->platform(),
                'deviceType'        => $agent->deviceType(),
                'browser'           => $agent->browser() . " " . $agent->version($agent->browser()),
            ];

            // If this short URL is commercial, then needed show commercial image
            if ($url->commercial) {

                // get all files from storage
                $files = Storage::disk('commercial-images')->files();

                // array mixing
                shuffle($files);

                // get random image
                $statisticData['showed_commercial_file'] = $files[0];

                // create log for current request (with a filename which will be shown)
                $url->statistics()->create($statisticData);

                // return layout included vue app with a filename which will be shown
                return view('layout', [
                    'file' => Storage::disk('commercial-images')->url($files[0]),
                    'address' => $url->url
                ]);
            }

            // create log for current request
            $url->statistics()->create($statisticData);
            return redirect()->away($url->url, $url->couldExpire() ? 302 : 301);
        }

        abort(404);
    }
}