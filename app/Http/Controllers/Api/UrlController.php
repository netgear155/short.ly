<?php

namespace App\Http\Controllers\Api;

use App\Hasher;
use App\Http\Controllers\Controller;
use App\Http\Requests\UrlRequest;
use App\Http\Resources\Url\UrlResource;
use Carbon\Carbon;
use App\Models\Url;

class UrlController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param UrlRequest $request
     *
     * @return UrlResource
     */
    public function store(UrlRequest $request)
    {
        $data = [
            'url' => $request->get('url'),
            'code' => $request->get('code') ? \Str::slug($request->get('code')) : (new Hasher(6))->generate(),
        ];

        if ($request->filled('expires_at')) {
            $data['expires_at'] = Carbon::parse($request->get('expires_at'))->toDateTimeString();
        }

        if ($request->filled('commercial')) {
            $data['commercial'] = $request->get('commercial');
        }

        $url = Url::create($data);
        return new UrlResource($url);
    }
}