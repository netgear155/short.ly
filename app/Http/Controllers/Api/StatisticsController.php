<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Statistics\StatisticsCollection;
use App\Http\Resources\Url\{
    UrlCollection,
    UrlResource
};
use App\Models\{
    Url,
    UrlStatistic
};
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{
    public function index() {
        // paginate URL models
        $urls = Url::paginate(10);

        // get a counting of for unique visitors shorter URL
        $urlsStatistics = UrlStatistic::whereIn('url_id', $urls->pluck('id'))
            ->distinct()
            ->select(DB::raw('visitor_ip, url_id, device, browser'))
            ->get()
            ->groupBy('url_id')
            ->map(function ($item) {
                return $item->count();
            });

        // return ApiResource (URL models, urlStatisticsCount)
        return new UrlCollection($urls, $urlsStatistics);
    }

    public function single(Request $request, $code) {
        // new Hashids singleton for decode hash code with id for URL
        $hashids = new Hashids('', 6);
        $hashids = $hashids->decode($code);

        // search URL model by id
        $url = Url::find($hashids[0]);
        // get statistics for current URL
        $urlStatistics = $url->statistics()->orderBy('created_at')->paginate(config('urls.items_per_page'));

        // return ApiResource (URL model, urlStatistics)
        return new StatisticsCollection($urlStatistics, new UrlResource($url));
    }
}
