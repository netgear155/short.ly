<?php

namespace App\Http\Resources\Url;

use App\Models\Url;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UrlCollection extends ResourceCollection
{
    /**
     * @var Url
     */
    protected $urlsStatistics;

    /**
     * Create a new resource instance.
     *
     * @param  mixed  $resource
     * @return void
     */
    public function __construct($resource, $urlsStatistics)
    {
        parent::__construct($resource);

        $this->resource = $this->collectResource($resource);
        $this->urlsStatistics = $urlsStatistics;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'items' => $this->collection,
            'urlsStatistics' => $this->urlsStatistics
        ];
    }
}
