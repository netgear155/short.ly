<?php

namespace App\Http\Resources\Url;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UrlResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'code'          => $this->code,
            'url'           => $this->url,
            'shorterUrl'    => $this->shorterUrl,
            'expires_at'    => $this->expires_at !== null ? $this->expires_at->format('Y-m-d\TH:m') : null,
            'statisticsUrl' => $this->statisticsUrl,
            'commercial'    => $this->commercial,
        ];
    }
}
