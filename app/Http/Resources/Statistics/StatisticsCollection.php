<?php

namespace App\Http\Resources\Statistics;

use App\Models\Url;
use Illuminate\Http\Resources\Json\ResourceCollection;

class StatisticsCollection extends ResourceCollection
{
    /**
     * @var Url
     */
    protected $item;

    /**
     * Create a new resource instance.
     *
     * @param  mixed  $resource
     * @return void
     */
    public function __construct($resource, $item)
    {
        parent::__construct($resource);

        $this->resource = $this->collectResource($resource);
        $this->item = $item;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'items' => $this->collection,
            'url' => $this->item
        ];
    }
}
