<?php

namespace App\Http\Resources\Statistics;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class StatisticsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "visitor_ip" => $this->visitor_ip,
            "visitor_unique" => $this->visitor_unique,
            "showed_commercial_file" => Storage::disk('commercial-images')->url($this->showed_commercial_file),
            "created_at" => $this->created_at->format('Y-m-d\TH:m'),
            "browser" => $this->browser,
            "deviceType" => $this->deviceType,
            "device" => $this->device,
            "platform" => $this->platform
        ];
    }
}
