<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddedColumnsForUserAgents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('url_statistics', function (Blueprint $table) {
            $table->dropColumn('user_agent');
            $table->string('browser', 100)->nullable();
            $table->string('deviceType', 63)->nullable();
            $table->string('device', 63)->nullable();
            $table->string('platform', 63)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('url_statistics', function (Blueprint $table) {
            $table->dropColumn(['browser', 'device', 'deviceType', 'platform']);
            $table->json('user_agent')->nullable();
        });
    }
}
