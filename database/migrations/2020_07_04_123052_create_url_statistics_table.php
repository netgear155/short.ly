<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUrlStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('url_statistics', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('url_id');
            $table->ipAddress('visitor_ip')->nullable();
            $table->boolean('visitor_unique')->nullable()->default(false);
            $table->json('user_agent')->nullable();
            $table->string('showed_commercial_file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('url_statistics');
    }
}
